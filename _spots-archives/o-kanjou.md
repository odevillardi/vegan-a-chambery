---
layout: spot
menu: spots
favori: oui

spotid: o-kanjou
title: "<strong>Ô Kanjou</strong>"
nom: "Ô Kanjou"

description: "Dans le restaurant asiatique *Ô Kanjou*, on aime énormément le **Ramen** et le **Pad Thai**, tous les deux disponibles en **versions véganes**."

type: Restaurant asiatique
type-class: restaurant

vegan: Options véganes
prix: "€<span class='light-gray-text'>€€</span>"
emporter: Plats à emporter

img: /dist/img/spots/o-kanjou.jpg
alt-img: "Devanture du restaurant Ô Kanjou à Chambéry"

img-logo: /dist/img/spots/o-kanjou-logo.jpg
alt-img-logo: "Logo Ô Kanjou à Chambéry"

liens:
  - site:
    nom: Site Web
    linkurl: http://www.okanjou.fr/
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/OKanjou-132420487355542/

adresse: "33 Rue de la Gare"
code-postal: "73000"
ville: "Chambéry"

tel: "09 77 54 98 28"
tel-int: "+33977549828"

horaires:
  - lundi: "Fermé"
  - mardi: "11h30 - 13h30 / 18h30 - 21h"
  - mercredi: "11h30 - 13h30 / 18h30 - 21h"
  - jeudi: "11h30 - 13h30 / 18h30 - 21h"
  - vendredi: "11h30 - 13h30 / 18h30 - 21h"
  - Samedi: "11h30 - 13h30"
  - dimanche: "Fermé"

related:
  - spot1:
    url: my-little-warung
    nom: My Little Warung
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.918581187725068%2C45.568735798978516%2C5.9204426407814035%2C45.57011588324018&amp;layer=mapnik&amp;marker=45.56942490651521%2C5.919511914253235"></iframe>
