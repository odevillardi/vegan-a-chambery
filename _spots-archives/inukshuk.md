---
layout: spot
menu: spots

spotid: inukshuk
title: "<strong>Inukshuk</strong>"
nom: "Inukshuk"

description: "Chez *Inukshuk*, on apprécie particulièrement le **plat du jour** souvent **végan** et le chef très arrangeant quand ce n’est pas le cas. Quelques **desserts végans** sont aussi parfois à la carte."

type: Restaurant & café vélo
type-class: restaurant

vegan: Options véganes
prix: "€€<span class='light-gray-text'>€</span>"
emporter: Plats à emporter

img: /dist/img/spots/inukshuk.jpg
alt-img: "Bar du restaurant Inukshuk à Chambéry"

img-logo: /dist/img/spots/inukshuk-logo.jpg
alt-img-logo: "Logo Inukshuk à Chambéry"

liens:
  - site:
    nom: Site Web
    linkurl: http://www.inukshuk-cafe.fr/
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/inukshukcafevelo/
  - instagram:
    nom: Instagram
    linkurl: https://www.instagram.com/inukshuk_cyclable/

adresse: "45 place de la Brigade de Savoie"
code-postal: "73000"
ville: "Chambéry"

tel: "04 58 14 07 59"
tel-int: "+33458140759"

horaires:
  - lundi: "Fermé"
  - mardi: "10h - 13h / 14h - 19h"
  - mercredi: "14h – 19h"
  - jeudi: "10h - 13h / 14h - 19h"
  - vendredi: "10h - 13h / 14h - 19h"
  - Samedi: "10h - 13h / 14h - 19h"
  - dimanche: "Fermé"

related:
  - spot1:
    url: chez-gaia
    nom: Chez Gaïa
  - spot2:
    url: arctic-juice-cafe
    nom: Arctic Juice & Café
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.924270153045655%2C45.56316442023494%2C5.92677801847458%2C45.56455403052397&amp;layer=mapnik&amp;marker=45.56385922967603%2C5.925525426864624"></iframe>
