---
layout: spot
menu: spots

spotid: mamilitante
title: "<strong>Mamilitante</strong>"
nom: "Mamilitante"

description: "*Mamilitante* est un projet de pâtisserie savoyarde engagée financée en Crowdfunding. On a hâte de goûter leurs pâtisseries rapidement&nbsp;!"

type: Pâtisserie savoyarde engagée
type-class: cafe

vegan: 100% végan
prix: "€€<span class='light-gray-text'>€</span>"
emporter: Click and Collect

img: /dist/img/spots/mamilitante.jpg
alt-img: "Logo Mamilitante"

img-logo: /dist/img/spots/mamilitante-logo.jpg
alt-img-logo: "Logo Mamilitante"

liens:
  - Site Web:
    nom: Site Web
    linkurl: https://mamilitante.fr/
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/patisserie.mamilitante/
  - instagram:
    nom: Instagram
    linkurl: https://www.instagram.com/patisserie.mamilitante/

adresse: "Click and Collect"
code-postal: "73000"
ville: "Chambéry"

tel: 06 01 99 14 21

horaires:
  - lundi: "Ouvert"
  - mardi: "Ouvert"
  - mercredi: "Ouvert"
  - jeudi: "Ouvert"
  - vendredi: "Ouvert"
  - Samedi: "Ouvert"
  - dimanche: "Fermé"

related:
  - spot1:
    url: dont-panic
    nom: Don’t panic
  - spot2:
    url: silene-della-libera
    nom: Silène Della Libera
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.8579444885253915%2C45.526795118388684%2C6.017417907714844%2C45.6157184378405&amp;layer=mapnik"></iframe>
