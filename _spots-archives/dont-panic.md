---
layout: spot
menu: spots
favori: oui

spotid: dont-panic
title: "<strong>Don’t Panic</strong>"
nom: "Don’t Panic"

description: "*Don’t Panic* est une grande **épicerie VRAC** avec un très beau rayon de produits alternatifs **100% végétaux**."

type: Épicerie vrac
type-class: commerce

vegan: "100% végan"
prix: "€<span class='light-gray-text'>€€</span>"

emporter: Uniquement à emporter

img: /dist/img/spots/dont-panic.jpg
alt-img: "Vitrine de l'épicerie Don’t Panic"

img-logo: /dist/img/spots/dont-panic-logo.jpg
alt-img-logo: "Logo Don’t Panic"

liens:
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/DontPanicVrac/

adresse: "95 Rue Sommeiller"
code-postal: "73000"
ville: "Chambéry"

tel: "06 48 24 72 98"
tel-int: "+33648247298"

horaires:
  - lundi: "10h - 13h30 / 14h30 - 19h"
  - mardi: "10h - 13h30 / 14h30 - 19h"
  - mercredi: "10h - 13h30 / 14h30 - 19h"
  - jeudi: "10h00 - 13h30 / 14h30 - 19h"
  - vendredi: "10h00 - 19h00"
  - Samedi: "10h00 - 19h00"
  - dimanche: "Fermé"

related:
  - spot1:
    url: chez-gaia
    nom: Chez Gaïa
  - spot1:
    url: in-bloom
    nom: In Bloom
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.918267369270325%2C45.56782699028121%2C5.92340111732483%2C45.570605945819395&amp;layer=mapnik&amp;marker=45.56921648523651%2C5.920834243297577"></iframe>
