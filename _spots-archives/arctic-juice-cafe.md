---
layout: spot
menu: spots

spotid: arctic-juice-cafe
title: "<strong>Arctic Juice & Café</strong>"
nom: "Arctic Juice & Café"

description: "Chez *Arctic*, on savoure **les jus quasiment tous végans** et on dévore les quelques **sandwiches et bowls végans** eux aussi à la carte."

type: Café & restauration rapide
type-class: cafe

favori: oui

vegan: Options véganes
prix: "€€<span class='light-gray-text'>€</span>"
emporter: Jus et plats à emporter

img: /dist/img/spots/arctic-juice-cafe.jpg
alt-img: "Devanture du café Arctic Juice & Café à Chambéry"

img-logo: /dist/img/spots/arctic-juice-cafe-logo.jpg
alt-img-logo: "Logo Arctic Juice & Café à Chambéry"

liens:
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/ArcticChambery/

adresse: "14 rue du Sénat de Savoie"
code-postal: "73000"
ville: "Chambéry"

tel: "04 79 60 66 94"
tel-int: "+33479606694"

horaires:
  - lundi: "10h - 17h"
  - mardi: "10h - 17h"
  - mercredi: "10h - 17h"
  - jeudi: "10h - 17h"
  - vendredi: "10h - 17h"
  - Samedi: "10h - 18h"
  - dimanche: "Fermé"

related:
  - spot1:
    url: mylk-tea-cafe
    nom: Mylk Tea Café
  - spot2:
    url: chez-gaia
    nom: Chez Gaïa
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.918527543544769%2C45.56556241538733%2C5.921035408973694%2C45.56695196636049&amp;layer=mapnik&amp;marker=45.566257195170465%2C5.919782817363739"></iframe>
