---
layout: spot
menu: spots

spotid: la-petite-maison
title: "<strong>La Petite Maison</strong>"
nom: "La petite maison"

description: "Au restaurant *La petite maison*, on aime le menu complet “Entrée, plat & dessert” **entièrement végan** qui change au gré des saisons."

type: Restaurant
type-class: restaurant

vegan: Options véganes
prix: "€€€"
emporter: Plats à emporter

img: /dist/img/spots/la-petite-maison.jpg
alt-img: "Devanture du restaurant la petite maison à Chambéry"

img-logo: /dist/img/spots/la-petite-maison-logo.jpg
alt-img-logo: "Logo la petite maison à Chambéry"

adresse: "50 avenue des Bernardines"
code-postal: "73000"
ville: "Chambéry"

tel: "04 79 69 22 98"
tel-int: "+33479692298"

horaires:
  - lundi: "11h30—13h30"
  - mardi: "11h30—13h30"
  - mercredi: "11h30—13h30"
  - jeudi: "11h30—13h30"
  - vendredi: "11h30—13h30"
  - Samedi: "Fermé"
  - dimanche: "Fermé"

related:
  - spot1:
    url: "cafe-des-inities"
    nom: "Café des initiés"
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.914463996887208%2C45.56523380033442%2C5.9169718623161325%2C45.56662335943621&amp;layer=mapnik&amp;marker=45.56592858418189%2C5.915716588497162"></iframe>
