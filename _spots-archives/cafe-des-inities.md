---
layout: spot
menu: spots

spotid: cafe-des-inities
title: "<strong>Café des initiés</strong>"
nom: "Café des initiés"

description: "Au *Café des initiés*, on aime **le choix végan** aussi bien en entrée qu’en plat. Mention spéciale aux **Samosas** et au **Tofu Pasinda**, deux régalades."

type: Restaurant du monde
type-class: restaurant

vegan: Options véganes
prix: "€€<span class='light-gray-text'>€</span>"
emporter: Plats à emporter

img: /dist/img/spots/cafe-des-inities.jpg
alt-img: "Devanture du restaurant Café des initiés à Chambéry"

img-logo: /dist/img/spots/cafe-des-inities-logo.jpg
alt-img-logo: "Logo Café des initiés à Chambéry"

liens:
  - site:
    nom: Site Web
    linkurl: https://www.lecafedesinities73.fr
  - facebook:
    nom: Facebook
    linkurl: https://fr-fr.facebook.com/cafedesinities/

adresse: "8 Rue Derrière les Murs"
code-postal: "73000"
ville: "Chambéry"

tel: "04 79 25 98 28"
tel-int: "+33479259828"

horaires:
- lundi: "11h - 14h / 18h - 21h"
- mardi: "12h - 14h / 18h - 21h"
- mercredi: "12h - 14h / 18h - 21h"
- jeudi: "12h - 14h / 18h - 21h"
- vendredi: "12h - 14h / 18h - 21h"
- Samedi: "12h - 14h / 18h - 21h"
- dimanche: "Fermé"

related:
  - spot1:
    url: la-petite-maison
    nom: La petite maison
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.916936993598938%2C45.565605604651346%2C5.9194448590278625%2C45.566995154556196&amp;layer=mapnik&amp;marker=45.56630130002225%2C5.918191999999976"></iframe>
