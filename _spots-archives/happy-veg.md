---
layout: spot
menu: spots

savoie: oui

spotid: happy-veg
title: "<strong>Happy Veg</strong>"
nom: "Happy Veg"

description: "*Happy Veg* est une grande **épicerie 100% végane** avec beaucoup de choix notamment en terme de **similis et fromages végétaux**."

type: Épicerie végétale
type-class: commerce

vegan: "100% végan"
prix: "€<span class='light-gray-text'>€€</span>"

emporter: Uniquement à emporter

img-logo: /dist/img/spots/happy-veg-logo.jpg
alt-img-logo: "Logo Don’t Panic"

liens:
  - site:
    nom: Site web
    linkurl: https://www.happyveg.fr/
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/Happy-Veg-473936016357389/

adresse: "1 allée des nielles"
code-postal: "74600"
ville: "Annecy"

tel: "04 85 46 14 35"
tel-int: "+33485461435"

horaires:
  - lundi: "Fermé"
  - mardi: "10h00 - 19h00"
  - mercredi: "10h00 - 14h00"
  - jeudi: "10h00 - 19h00"
  - vendredi: "10h00 - 19h00"
  - Samedi: "10h00 - 19h00"
  - dimanche: "Fermé"

related:
  - spot1:
    url: dont-panic
    nom: Don’t Panic
  - spot1:
    url: mylk-tea-cafe
    nom: Mylk Tea Café
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=6.074345111846924%2C45.884138253760376%2C6.08818531036377%2C45.89528008897773&amp;layer=mapnik&amp;marker=45.889708550070736%2C6.08125455000004"></iframe>
