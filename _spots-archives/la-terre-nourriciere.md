---
layout: spot
menu: spots

spotid: la-terre-nourriciere
title: "<strong>La Terre Nourriciere</strong>"
nom: "La Terre Nourricière"

description: "*La Terre Nourricière* est un petit stand de plats et boissons **100% végans** à emporter, on aime leur **cuisine maison** et leurs **boissons chaudes** réconfortantes."

type: Restauration rapide
type-class: restaurant

vegan: "100% végan"
prix: "€<span class='light-gray-text'>€€</span>"
emporter: Uniquement à emporter

img: /dist/img/spots/la-terre-nourriciere.jpg
alt-img: "Devanture du restaurant La Terre Nourricière à Chambéry"

img-logo: /dist/img/spots/la-terre-nourriciere-logo.jpg
alt-img-logo: "Logo La Terre Nourricière à Chambéry"

liens:
  - site:
    nom: Site web
    linkurl: https://laterrenourriciere.fr
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/LaTerreNourriciereDeChambery/
  - instagram:
    nom: Instagram
    linkurl: https://www.instagram.com/laterrenourriciere/

adresse: "119 rue Croix d’or"
code-postal: "73000"
ville: "Chambéry"

tel: "09 80 97 67 18"
tel-int: "+33980976718"

horaires:
  - lundi: "Fermé"
  - mardi: "11h — 15h"
  - mercredi: "11h — 15h"
  - jeudi: "11h — 15h"
  - vendredi: "11h — 15h"
  - Samedi: "11h — 15h"
  - dimanche: "Fermé"

related:
  - spot1:
    url: chez-gaia
    nom: Chez Gaïa
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.921443104743958%2C45.56368271133497%2C5.923950970172883%2C45.56507230880394&amp;layer=mapnik&amp;marker=45.564377514366015%2C5.922695696353912"></iframe>
