---
layout: spot
menu: spots

spotid: my-little-warung
title: "<strong>My Little Warrung</strong>"
nom: "My Little Warung"

description: "À *Little Warung*, on aime pouvoir **composer nos plats végans** simples et très gouteux. On aime beaucoup le **Bali** (Curry Vert) et le **Nha Trang** (Phô)."

type: Restaurant asiatique
type-class: restaurant

vegan: Options véganes
prix: "€<span class='light-gray-text'>€€</span>"
emporter: Plats à emporter

img: /dist/img/spots/my-little-warung.jpg
alt-img: "Devanture du restaurant My Little Warung à Chambéry"

img-logo: /dist/img/spots/my-little-warung-logo.jpg
alt-img-logo: "Logo My Little Warung à Chambéry"

liens:
  - site:
    nom: Site Web
    linkurl: https://www.mylittlewarung.com
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/MyLittleWarungChambery/
  - instagram:
    nom: Instagram
    linkurl: https://www.instagram.com/mylittlewarung/

adresse: "23 rue de la République"
code-postal: "73000"
ville: "Chambéry"

tel: "04 79 62 07 88"
tel-int: "+33479620788"

horaires:
  - lundi: "12h — 14h"
  - mardi: "12h — 14h"
  - mercredi: "12h — 14h / 18h30 — 22h"
  - jeudi: "12h — 14h / 18h30 — 22h"
  - vendredi: "12h — 14h / 18h30 — 22h"
  - Samedi: "18h30 — 22h"
  - dimanche: "18h30 — 22h"

related:
  - spot1:
    url: o-kanjou
    nom: Ô Kanjou
---

<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.921601355075836%2C45.56307616004219%2C5.924109220504761%2C45.56446577251433&amp;layer=mapnik&amp;marker=45.56377097057482%2C5.922853946685791"></iframe>
