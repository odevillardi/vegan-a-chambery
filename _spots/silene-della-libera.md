---
layout: spot
description: "Les pâtisseries de *Silène Della Libera* sont hyper gourmandes, parmi nos favoris&nbsp;: les cookies cœur fondant et ceux au caramel salé."
type-class: cafe
vegan: 100% végan
related:
  - spot1: null
    url: mylk-tea-cafe
    nom: Mylk Tea Café
  - spot2: null
    url: chez-gaia
    nom: Chez Gaïa
nom: Silène Della Libera
code-postal: "73000"
horaires:
  - lundi: Ouvert
  - mardi: Ouvert
  - mercredi: Ouvert
  - jeudi: Ouvert
  - vendredi: Ouvert
  - Samedi: Fermé
  - dimanche: Fermé
liens:
  - site: null
    nom: Site Web
    linkurl: https://silenedellalibera.wordpress.com
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/sdl.patisseries/
  - instagram: null
    nom: Instagram
    linkurl: https://www.instagram.com/sdl.patisseries/
type: Pâtisserie bio et végétale
prix: €€<span class='light-gray-text'>€</span>
emporter: En livraison uniquement
adresse: En livraison uniquement
ville: Chambéry
title: <strong>Silène Della Libera</strong>
tel: 06 02 36 65 27
spotid: silene-della-libera
tel-int: "+33602366527"
menu: spots
img: /dist/img/spots/silene-della-libera.jpg
alt-img: Pâtisserie de Silène Della Libera
img-logo: /dist/img/spots/silene-della-libera-logo.jpg
alt-img-logo: Logo de Silène Della Libera
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.8579444885253915%2C45.526795118388684%2C6.017417907714844%2C45.6157184378405&amp;layer=mapnik"></iframe>
