---
layout: spot
menu: spots

savoie: oui

spotid: mylk-tea-cafe
title: "<strong>Mylk Tea Café</strong>"
nom: "Mylk Tea Café"

description: "Chez *Mylk Tea Café*, on aime les délicieuses **boissons chaudes végétales** et les plats faits maison et **véganisés** à la demande."

type: Salon de thé
type-class: cafe

vegan: Options véganes
prix: "€€<span class='light-gray-text'>€</span>"

img: /dist/img/spots/mylk-tea-cafe.jpg
alt-img: "Intérieur du restaurant Mylk Tea Café à Annecy"

img-logo: /dist/img/spots/mylk-tea-cafe-logo.jpg
alt-img-logo: "Logo Mylk Tea Café à Annecy"

liens:
  - site:
    nom: Site Web
    linkurl: https://mylk-tea-cafe.business.site
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/mylkteacafe/

adresse: "34 rue sommeiller"
code-postal: "74000"
ville: "Annecy"

tel: "06 75 16 63 85"
tel-int: "+33675166385"

horaires:
  - Lundi: "9h - 12h / 15h - 17h"
  - Mardi: "9h - 12h / 15h - 17h"
  - Mercredi: "9h - 12h / 15h - 17h"
  - Jeudi: "9h - 12h / 15h - 17h"
  - Vendredi: "9h - 12h / 15h - 17h"
  - Samedi: "9h - 12h / 15h - 17h"
  - Dimanche: "Fermé"

related:
  - spot1:
    url: arctic-juice-cafe
    nom: Arctic Juice & Café
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=6.12463653087616%2C45.90154825563821%2C6.127144396305085%2C45.90292947178694&amp;layer=mapnik&amp;marker=45.90223886800784%2C6.1258891224861145"></iframe>
