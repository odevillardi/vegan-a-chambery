---
layout: spot
menu: spots

spotid: corsaire
title: "<strong>Le Corsaire</strong>"
nom: "Le Corsaire"

description: "Le Corsaire propose une planche **100% vegan** composée de frites de patates douces, de pain avec une tartinade, fleurs de courgettes frites et crudités."

type: Bar
type-class: bar

vegan: Options véganes
prix: "€€<span class='light-gray-text'>€</span>"

img: /dist/img/spots/corsaire.jpg
alt-img: "Intérieur du bar Le Corsaire à Chambéry"

img-logo: /dist/img/spots/corsaire-logo.jpg
alt-img-logo: "Logo du bar Le Corsaire à Chambéry"

liens:
  - site:
    nom: Site Web
    linkurl: https://www.corsaire-chambery.com
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/LE-CORSAIRE-Beer-Rooftop-Bar-647217175344980/

adresse: "20 Av. des Ducs de Savoie"
code-postal: "73000"
ville: "Chambéry"

tel: "07 83 09 73 38"
tel-int: "+33783097338"

horaires:
- lundi: "17h - 00h45"
- mardi: "17h - 01h30"
- mercredi: "17h - 01h30"
- jeudi: "17h - 01h30"
- vendredi: "17h - 01h30"
- Samedi: "17h - 01h30"
- dimanche: "17h - 00h45"

related:
  - spot1:
    url: beer-o-clock
    nom: Beer O Clock
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.918240547180177%2C45.56561874920339%2C5.925160646438599%2C45.57113918913218&amp;layer=mapnik&amp;marker=45.56837903698684%2C5.921700596809387"></iframe>
