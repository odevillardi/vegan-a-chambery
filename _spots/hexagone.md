---
layout: spot
menu: spots

spotid: hexagone
title: "<strong>Hexagone</strong>"
nom: "Hexagone"

description: "On vient chez *Hexagone* évidemment pour **les magnifiques pains** et leur sens de l’humour, mais surtout pour le **Passepartout aux 2 Olives**."

type: Boulangerie
type-class: cafe

vegan: Options véganes
prix: "€<span class='light-gray-text'>€€</span>"
emporter: Plats à emporter

img: /dist/img/spots/hexagone.jpg
alt-img: "Devanture de la boulangerie Hexagone à Chambéry"

img-logo: /dist/img/spots/hexagone-logo.jpg
alt-img-logo: "Logo Hexagone à Chambéry"

liens:
  - site:
    nom: Site Web
    linkurl: https://boulangerie-hexagone.fr
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/BoulangerieHexagone/
  - instagram:
    nom: Instagram
    linkurl: https://www.instagram.com/boulangeriehexagone/

adresse: "5 rue Bonivard"
code-postal: "73000"
ville: "Chambéry"

tel: "04 79 79 05 87"
tel-int: "+33479790587"

horaires:
  - lundi: "07h — 20h"
  - mardi: "07h — 20h"
  - mercredi: "07h — 20h"
  - jeudi: "07h — 20h"
  - vendredi: "07h — 20h"
  - Samedi: "07h — 20h"
  - dimanche: "07h — 20h"

related:
  - spot1:
    url: chez-gaia
    nom: Chez Gaïa
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.918125212192536%2C45.565566170976815%2C5.920633077621461%2C45.56695572185707&amp;layer=mapnik&amp;marker=45.56626095071353%2C5.919379144906998"></iframe>
