---
layout: spot
menu: spots

favori: oui

spotid: in-bloom
nom: "In Bloom"
title: "<strong>In bloom</strong>"

description: "Chez *In Bloom*, tout est **100% végan** et il y a aussi beaucoup de **zéro déchet**, c’est l’endroit parfait pour faire le plein de **cosmétiques éthiques**."

type: Boutique de cosmétiques
type-class: commerce

vegan: "100% végan"
prix: "€€<span class='light-gray-text'>€</span>"
emporter: Service à emporter

img: /dist/img/spots/in-bloom.jpg
alt-img: "Devanture de la boutique In Bloom à Chambéry"

img-logo: /dist/img/spots/in-bloom-logo.jpg
alt-img-logo: "Logo In Bloom à Chambéry"

liens:
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/inbloomcosmetiques/

adresse: "53 Rue Croix d’Or"
code-postal: "73000"
ville: "Chambéry"

tel: "06 78 17 21 36"
tel-int: "+33678172136"

horaires:
  - lundi: "Fermé"
  - mardi: "10h30 — 19h"
  - mercredi: "10h30 — 19h"
  - jeudi: "10h30 — 19h"
  - vendredi: "10h30 — 19h"
  - Samedi: "10h30 — 19h"
  - dimanche: "Fermé"

related:
  - spot1:
    url: chez-gaia
    nom: Chez Gaïa
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.922491848468781%2C45.56386486323096%2C5.924329161643982%2C45.56525445619437&amp;layer=mapnik&amp;marker=45.56455966400924%2C5.923409163951874"></iframe>
