---
layout: spot
menu: spots

spotid: french-coffee-shop
title: "<strong>French Coffee Shop</strong>"
nom: "French Coffee Shop"

description: "Chez *French Coffee Shop*, on savoure les cafés gourmands avec du lait végétal accompagnés —&nbsp;évidemment&nbsp;!&nbsp;— du fondant au chocolat **100% végétal**."

type: Café
type-class: cafe

favori: oui

vegan: Options véganes
prix: "€<span class='light-gray-text'>€€</span>"
emporter: Cafés et pâtisseries à emporter

img: /dist/img/spots/french-coffee-shop.jpg
alt-img: "Devanture du café French Coffee Shop à Chambéry"

img-logo: /dist/img/spots/french-coffee-shop-logo.jpg
alt-img-logo: "Logo French Coffee Shop"

liens:
  - site:
    nom: Site web
    linkurl: https://frenchcoffeeshop.com/
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/fcschambery/

adresse: "21 Place Saint-Léger"
code-postal: "73000"
ville: "Chambéry"

tel: "09 53 59 68 29"
tel-int: "+33953596829"

horaires:
  - lundi: "10h - 17h"
  - mardi: "10h - 17h"
  - mercredi: "10h - 17h"
  - jeudi: "10h - 17h"
  - vendredi: "10h - 17h"
  - Samedi: "10h - 18h"
  - dimanche: "Fermé"

related:
  - spot1:
    url: mylk-tea-cafe
    nom: Mylk Tea Café
  - spot2:
    url: chez-gaia
    nom: Chez Gaïa
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.919951796531678%2C45.562674292730726%2C5.923411846160889%2C45.56547603675967&amp;layer=mapnik&amp;marker=45.564075182211305%2C5.921681821346283"></iframe>
