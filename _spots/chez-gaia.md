---
layout: spot
menu: spots

favori: oui

spotid: chez-gaia
title: "<strong>Chez Gaïa</strong>"
nom: "Chez Gaïa"

description: "*Chez Gaïa* est le premier restaurant **100% végan** de Chambéry, on aime leur **cuisine végétale maison** et leurs **pâtisseries véganes** super gourmandes."

type: Restauration rapide
type-class: restaurant

vegan: "100% végan"
prix: "€€<span class='light-gray-text'>€</span>"
emporter: Plats à emporter

img: /dist/img/spots/chez-gaia.jpg
alt-img: "Intérieur du restaurant Chez Gaïa à Chambéry"

img-logo: /dist/img/spots/chez-gaia-logo.jpg
alt-img-logo: "Logo Chez Gaïa à Chambéry"

liens:
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/Gaiachambery/

adresse: "39 rue Juiverie"
code-postal: "73000"
ville: "Chambéry"

tel: "04 79 71 55 43"
tel-int: "+33479715543"

horaires:
  - lundi: "Fermé"
  - mardi: "10h—17h"
  - mercredi: "9h—17h"
  - jeudi: "9h—17h"
  - vendredi: "9h—17h"
  - Samedi: "9h—18h"
  - dimanche: "Fermé"

related:
  - spot1:
    url: arctic-juice-cafe
    nom: Arctic Juice & Café
  - spot1:
    url: in-bloom
    nom: In Bloom
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.918012559413911%2C45.56492959497203%2C5.920520424842835%2C45.56631916159861&amp;layer=mapnik&amp;marker=45.565624382581866%2C5.919265151023865"></iframe>
