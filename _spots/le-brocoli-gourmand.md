---
layout: spot
menu: spots

savoie: oui

spotid: le-brocoli-gourmand
title: "<strong>Le Brocoli Gourmand</strong>"
nom: "Le Brocoli Gourmand"

description: "*Le brocoli gourmand* est un food truck de cuisine végétale éco-responsable dans les environs de Chambéry."

type: Food truck
type-class: restaurant

vegan: "100% végan"
prix: "€€<span class='light-gray-text'>€</span>"
emporter: Plats à emporter

img: /dist/img/spots/le-brocoli-gourmand.jpg
alt-img: "Logo Le brocoli gourmand"

img-logo: /dist/img/spots/le-brocoli-gourmand-logo.jpg
alt-img-logo: "Logo Le brocoli gourmand"

liens:
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/Le-Brocoli-Gourmand-106987447937647
  - instagram:
    nom: Instagram
    linkurl: https://instagram.com/Le%20Brocoli%20Gourmand/

adresse: "600 Route de Saint-Genix"
code-postal: "73240"
ville: "Sainte-Marie-d’Alvey"

tel: "06 89 45 08 46"
tel-int: "+33689450846"

horaires:
  - lundi: "Fermé"
  - mardi: "11h45 - 14h00"
  - mercredi: "11h45 - 14h00"
  - jeudi: "11h45 - 14h00"
  - vendredi: "11h45 - 14h00"
  - Samedi: "Fermé"
  - dimanche: "Fermé"

related:
  - spot1:
    url: chez-gaia
    nom: Chez Gaïa
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.698556900024415%2C45.58875590252204%2C5.726237297058106%2C45.61115553441939&amp;layer=mapnik&amp;marker=45.599956836273%2C5.71239709854126"></iframe>
