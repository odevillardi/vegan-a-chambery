---
layout: spot
menu: spots

spotid: beer-o-clock
title: "<strong>Beer O’Clock</strong>"
nom: "Beer O’Clock"

description: "Chez *Beer O’Clock*, un petit bar à bières, on apprécie évidemment **l’énorme choix de bières** mais aussi la petite **nouveauté végane** qui se mange : la **planche du jardin**."

type: Bar à bières
type-class: bar

vegan: Options véganes
prix: "€€<span class='light-gray-text'>€</span>"
emporter: Vente à emporter

img: /dist/img/spots/beer-o-clock.jpg
alt-img: "Devanture du bar Beer O’Clock à Chambéry"

img-logo: /dist/img/spots/beer-o-clock-logo.jpg
alt-img-logo: "Logo Beer O’Clock à Chambéry"

liens:
  - site:
    nom: Site Web
    linkurl: http://www.beeroclock.fr/
  - facebook:
    nom: Facebook
    linkurl: https://fr-fr.facebook.com/Beer-o-clock-Chambéry-1745269132368290/

adresse: "44 Place Saint Léger"
code-postal: "73000"
ville: "Chambéry"

tel: "09 52 60 82 15"
tel-int: "+33952608215"

horaires:
  - lundi: "Fermé"
  - mardi: "17h - 00h"
  - mercredi: "16h - 00h"
  - jeudi: "16h - 01h"
  - vendredi: "16h - 01h"
  - Samedi: "16h - 01h"
  - dimanche: "17h - 00h"

related:
  - spot1:
    url: cafe-des-inities
    nom: Café des initiés
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.920434594154359%2C45.563570039763455%2C5.922942459583283%2C45.56495964001941&amp;layer=mapnik&amp;marker=45.564264844187996%2C5.921689867973328"></iframe>
