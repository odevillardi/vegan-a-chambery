---
layout: spot
menu: spots

savoie: oui

spotid: code-couleur-vegetal
title: "<strong>Coiffeur Code Couleur</strong>"
nom: "Coiffeur Code Couleur"

description: "Votre salon de coiffure *Code Couleur Végétal* a choisi les gammes de produits Marcapar, Zao… pour leur parti-pris bio et végétal."

type: Coiffeur
type-class: commerce

favori: non

vegan: "100% végan"
prix: "€<span class='light-gray-text'>€€</span>"
emporter: Réservation en ligne

img: /dist/img/spots/code-couleur-vegetal.jpg
alt-img: "Intérieur du coiffeur code couleur végétal"

img-logo: /dist/img/spots/code-couleur-vegetal-logo.jpg
alt-img-logo: "Logo du coiffeur code couleur végétal"

liens:
  - facebook:
    nom: Facebook
    linkurl: https://www.facebook.com/Code-Couleur-Végétal-751316544934623/
  - twitter:
    nom: Twitter
    linkurl: https://twitter.com/CouleurVegetal

adresse: "Allée des comtes de Savoie"
code-postal: "73100"
ville: "Barberaz"

tel: "04 79 27 36 07"
tel-int: "+33479273607"

horaires:
  - lundi: "Fermé"
  - mardi: "09h - 19h"
  - mercredi: "09h - 19h"
  - jeudi: "09h - 19h"
  - vendredi: "09h - 19h"
  - Samedi: "08h30 - 17h"
  - dimanche: "Fermé"

related:
  - spot1:
    url: in-bloom
    nom: In Bloom
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.9423911571502686%2C45.562704338984254%2C5.94611406326294%2C45.56528074545965&amp;layer=mapnik&amp;marker=45.56399230002226%2C5.944251000000008"></iframe>
