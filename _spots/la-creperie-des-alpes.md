---
layout: spot
menu: spots

savoie: oui

favori: oui

spotid: la-creperie-des-alpes
title: "<strong>La Crêperie des Alpes</strong>"
nom: "La crêperie des Alpes"

description: "L’excellente *Crêperie des Alpes* à Aix-les-Bains propose une **crêpe salée végane** mais aussi un grand choix en sarasin sucré accompagné d’une **chantilly végane**. Un délice !"

type: Crêperie
type-class: restaurant

vegan: Options véganes
prix: "€€<span class='light-gray-text'>€</span>"

img: /dist/img/spots/la-creperie-des-alpes.jpg
alt-img: "Devanture du restaurant La crêperie des Alpes à Aix-les-Bains"

img-logo: /dist/img/spots/la-creperie-des-alpes-logo.jpg
alt-img-logo: "Logo La crêperie des Alpes à Aix-les-Bains"

liens:
  - site:
    nom: Site Web
    linkurl: http://lacreperiedesalpes.fr/
  - facebook:
    nom: Facebook
    linkurl: https://fr-fr.facebook.com/lacreperiedesalpes

adresse: "4 rue Albert 1er"
code-postal: "73100"
ville: "Aix-les-Bains"

tel: "04 79 61 36 85"
tel-int: "+33479613685"

horaires:
  - Lundi: "Fermé"
  - Mardi: "11h — 14h50 / 19h — 22h30"
  - Mercredi: "11h — 14h50 / 19h — 22h30"
  - Jeudi: "11h — 14h50 / 19h — 22h30"
  - Vendredi: "11h — 14h50 / 19h — 22h30"
  - Samedi: "11h — 14h50 / 19h — 22h30"
  - Dimanche: "Fermé"

related:
  - spot1:
    url: la-petite-maison
    nom: La petite maison
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.912833213806152%2C45.68808419348618%2C5.915341079235077%2C45.68947071057291&amp;layer=mapnik&amp;marker=45.68877745632571%2C5.914085805416107"></iframe>
